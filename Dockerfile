FROM bigdft/runtime:ubuntu18.04_cuda10.2_ompi_master
#ARG NB_USER=bigdftuser
#ARG NB_UID=1000
#ENV USER ${NB_USER}
#ENV NB_UID ${NB_UID}
#ENV HOME /home/${NB_USER}
#USER root
#RUN adduser --disabled-password \
#    --gecos "Default user" \
#    --uid ${NB_UID}  \
#    ${NB_USER}

# Make sure the contents of our repo are in ${HOME}
COPY tutorials ${HOME}
USER root
RUN chown -R 1000 ${HOME}
USER bigdft

